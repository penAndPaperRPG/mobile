package com.example.kpwarhammer

import android.app.ActionBar
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.abilities_item.view.*
import kotlinx.android.synthetic.main.action_bar_layout.*
import kotlinx.android.synthetic.main.activity_equipment.*

class EquipmentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_equipment)
        val id = intent.getStringExtra(KPSelectorActivity.ID_KEY)
        supportActionBar?.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM)
        supportActionBar?.setCustomView(R.layout.action_bar_layout)
        val textView = findViewById<TextView>(R.id.action_bar_layout_title)
        textView.text = "Ekwipunek"
        val button = findViewById<Button>(R.id.action_bar_layout_button)
        button.text = "Edytuj"
        action_bar_layout_button.setOnClickListener {
            val intent = Intent(this, EquipmentEditActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        action_bar_layout_next.setOnClickListener {
            val intent = Intent(this, RollActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        action_bar_layout_previous.setOnClickListener {
            val intent = Intent(this, AbilitiesActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        fetchEquipment(id)
    }

    fun fetchEquipment(id: String) {
        val adapter = GroupAdapter<ViewHolder>()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val weapons: Array<String> = arrayOf(
            "weapon1",
            "weapon2",
            "weapon3",
            "weapon4",
            "weapon5"
        )
        val armors: Array<String> = arrayOf(
            "armor1",
            "armor2",
            "armor3",
            "armor4",
            "armor5"
        )
        val db = FirebaseFirestore.getInstance()
        db.collection(email).document(id)
            .get()
            .addOnSuccessListener { result ->

                for (value in 1..5) {
                    if(result.get("weapon$value")!=null){
                    val weapon = result.get("weapon$value") as ArrayList<String>
                    var name :String=weapon.get(0)
                    var damage: String=weapon.get(1)
                    var minRange: String=weapon.get(2)
                    var maxRange: String=weapon.get(3)
                    var reload: String=weapon.get(4)
                    var traits: String=weapon.get(5)
                    if (traits == "" || traits=="null") traits == "brak"
                    if (name == "" || name == "null") {
                        adapter.add(EquipmentValueItem("Broń $value: brak broni", "red"))
                    } else
                        adapter.add(
                            EquipmentValueItem(
                                "Broń $value: $name, obrażenia: $damage, zas. min. $minRange, zas. max. $maxRange, przeładowanie: $reload, cechy szczególne: $traits",
                                "red"
                            )
                        )}
                    else{
                        adapter.add(EquipmentValueItem("Broń $value: brak", "red"))
                    }
                }
                for (value in 1..5) {
                    if(result.get("armor$value")!=null){
                    val armor = result.get("armor$value") as ArrayList<String>
                    var name = armor.get(0)
                    var health: String = armor.get(1)
                    if (name == ""  ||  name=="null") {
                        adapter.add(EquipmentValueItem("Zbroja $value: brak zbroi", "blue"))
                    } else
                        adapter.add(EquipmentValueItem("Zbroja $value: $name, punkty zbroi: $health", "blue"))
                }
                    else{
                        adapter.add(EquipmentValueItem("Zbroja $value: brak", "blue"))
                    }
                }
                if(result.get("wounds")!=null) {
                    val wounds = result.get("wounds") as ArrayList<String>
                    if (wounds.get(0) != "" && wounds.get(0) != "null") adapter.add(
                        EquipmentValueItem(
                            "Rany: ${wounds.get(
                                0
                            )}", "black"
                        )
                    )
                }
                else adapter.add(EquipmentValueItem("Rany: brak", "black"))
                recyclerview_equipment.adapter = adapter
            }
    }}
class EquipmentValueItem(private val equipment: String, color: String) : Item<ViewHolder>() {
val color=color
    override fun bind(viewHolder: ViewHolder, position: Int) {
        if(color=="red"){
            viewHolder.itemView.abilities_item_attr.setTextColor(Color.RED)
        }
        else if(color=="blue"){
            viewHolder.itemView.abilities_item_attr.setTextColor(Color.BLUE)
        }
        else if(color=="black"){
            viewHolder.itemView.abilities_item_attr.setTextColor(Color.BLACK)
        }
        else {
            viewHolder.itemView.abilities_item_attr.setTextColor(Color.DKGRAY)
        }
        viewHolder.itemView.abilities_item_attr.text = equipment
    }
    override fun getLayout(): Int {
        return R.layout.equipment_item
    }
}