package com.example.kpwarhammer

import android.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.abilities_item.view.*
import kotlinx.android.synthetic.main.action_bar_layout.*
import kotlinx.android.synthetic.main.activity_equipment.*
import kotlinx.android.synthetic.main.activity_equipment_edit.*
import kotlinx.android.synthetic.main.activity_stats_edit.*
import kotlinx.android.synthetic.main.equipment_edit_item_armor.view.*
import kotlinx.android.synthetic.main.equipment_edit_item_weapon.view.*
import kotlinx.android.synthetic.main.equipment_edit_item_wounds.view.*
import kotlinx.android.synthetic.main.stats_edit_item.view.*
import java.lang.NumberFormatException

class EquipmentEditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_equipment_edit)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.action_bar_layout)
        val textView = findViewById<TextView>(R.id.action_bar_layout_title)
        textView.text = "Edycja ekwipunku"
        val button = findViewById<Button>(R.id.action_bar_layout_button)
        button.text = "Zapisz"
        val id = intent.getStringExtra(KPSelectorActivity.ID_KEY)

        val equipmentMap: MutableMap<String, Any> = mutableMapOf<String, Any>()

        action_bar_layout_button.setOnClickListener {
            save(id, equipmentMap)
        }
        fetchEquipment(id)
    }

    fun save(id: String, equipmentMap: MutableMap<String, Any>) {
        val db = FirebaseFirestore.getInstance()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val weapons: Array<String> = arrayOf(
            "weapon1",
            "weapon2",
            "weapon3",
            "weapon4",
            "weapon5"
        )
        val armors: Array<String> = arrayOf(
            "armor1",
            "armor2",
            "armor3",
            "armor4",
            "armor5"
        )


        for(i in 1..5) {
            val recordName = "weapon$i"
            var name = recyclerview_equipment_edit.findViewHolderForLayoutPosition(i-1)
                ?.itemView?.equipment_edit_item_weapon_name?.text.toString()
            var traits = recyclerview_equipment_edit.findViewHolderForLayoutPosition(i-1)
                ?.itemView?.equipment_edit_item_weapon_traits?.text.toString()
            val damage = recyclerview_equipment_edit.findViewHolderForLayoutPosition(i-1)
                ?.itemView?.equipment_edit_item_weapon_damage?.text.toString()
            val minRange = recyclerview_equipment_edit.findViewHolderForLayoutPosition(i-1)
                ?.itemView?.equipment_edit_item_weapon_minrange?.text.toString()
            val maxRange = recyclerview_equipment_edit.findViewHolderForLayoutPosition(i-1)
                ?.itemView?.equipment_edit_item_weapon_maxrange?.text.toString()
            val reload = recyclerview_equipment_edit.findViewHolderForLayoutPosition(i-1)
                ?.itemView?.equipment_edit_item_weapon_reload?.text.toString()
            val list : List<String> = listOf<String>(name, traits, damage, minRange, maxRange, reload)
            equipmentMap[recordName]=list
        }
        for(i in 1..5){
            val recordName = "armor$i"
            var name = recyclerview_equipment_edit.findViewHolderForLayoutPosition(i+4)
                ?.itemView?.equipment_edit_item_armor_name?.text.toString()
            var health = recyclerview_equipment_edit.findViewHolderForLayoutPosition(i+4)
                ?.itemView?.equipment_edit_item_armor_health?.text.toString()
            val list: List<String> = listOf<String>(name, health)
            equipmentMap[recordName]=list
        }
        val wounds = recyclerview_equipment_edit.findViewHolderForLayoutPosition(10)
            ?.itemView?.equipment_edit_item_wounds?.text.toString()
        var woundsMap = wounds.split(", ", ",").toList()
        equipmentMap["wounds"]=woundsMap

        db.collection(email).document(id).update(equipmentMap)
            .addOnSuccessListener {
                Toast.makeText(this, "Zapisano!", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener {
                Toast.makeText(this, "Nie udało się zapisać! Błąd: $it", Toast.LENGTH_SHORT).show()
            }
    }

    fun fetchEquipment(id: String) {
        val adapter = GroupAdapter<ViewHolder>()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val db = FirebaseFirestore.getInstance()
        db.collection(email).document(id)
            .get()
            .addOnSuccessListener { result ->
                for (i in 1..5) {
                    if (result.get("weapon$i") != null) {
                        val list = result.get("weapon$i") as ArrayList<String>
                        var name = list.get(0)
                        var damage = list.get(1)
                        var minRange = list.get(2)
                        var maxRange = list.get(3)
                        var reload = list.get(4)
                        var traits = list.get(5)
                        adapter.add(WeaponValueItem(WeaponItem(name, damage, minRange, maxRange, reload, traits)))
                    } else adapter.add(WeaponValueItem(WeaponItem("", "", "", "", "", "")))
                }

                for (i in 1..5) {
                    if (result.get("armor$i") != null) {
                        val list = result.get("armor$i") as ArrayList<String>
                        val name = list.get(0)
                        val health = list.get(1)
                        adapter.add(ArmorValueItem(ArmorItem(name, health)))
                    } else {
                        adapter.add(ArmorValueItem(ArmorItem("", "")))
                    }
                }

                if (result.get("wounds") != null) {
                    val list = result.get("wounds") as ArrayList<String>
                    val wound = list.get(0)
                    adapter.add(WoundsValueItem(WoundsItem(wound)))
                } else {
                    adapter.add(WoundsValueItem(WoundsItem("")))
                }
            }
        recyclerview_equipment_edit.adapter = adapter
    }
}

class WeaponItem(
    val name: String,
    val damage: String,
    val minRange: String,
    val maxRange: String,
    val reload: String,
    val traits: String
)

class ArmorItem(val name: String, val health: String)
class WoundsItem(val wounds: String)

class WoundsValueItem(val woundsItem: WoundsItem) : Item<ViewHolder>() {
    val name = Editable.Factory.getInstance().newEditable(woundsItem.wounds.toString())
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.equipment_edit_item_wounds.text = name
    }

    override fun getLayout(): Int {
        return R.layout.equipment_edit_item_wounds
    }
}


class ArmorValueItem(val armorItem: ArmorItem) : Item<ViewHolder>() {
    val name = Editable.Factory.getInstance().newEditable(armorItem.name)
    val health = Editable.Factory.getInstance().newEditable(armorItem.health)

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.equipment_edit_item_armor_name.text = name
        viewHolder.itemView.equipment_edit_item_armor_health.text = health
    }

    override fun getLayout(): Int {
        return R.layout.equipment_edit_item_armor
    }
}

class WeaponValueItem(val weaponItem: WeaponItem) : Item<ViewHolder>() {

    val name = Editable.Factory.getInstance().newEditable(weaponItem.name)
    val damage = Editable.Factory.getInstance().newEditable(weaponItem.damage)
    val minRange = Editable.Factory.getInstance().newEditable(weaponItem.minRange)
    val maxRange = Editable.Factory.getInstance().newEditable(weaponItem.maxRange)
    val reload = Editable.Factory.getInstance().newEditable(weaponItem.reload)
    val traits = Editable.Factory.getInstance().newEditable(weaponItem.traits)

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.equipment_edit_item_weapon_name.text = name
        viewHolder.itemView.equipment_edit_item_weapon_damage.text = damage
        viewHolder.itemView.equipment_edit_item_weapon_minrange.text = minRange
        viewHolder.itemView.equipment_edit_item_weapon_maxrange.text = maxRange
        viewHolder.itemView.equipment_edit_item_weapon_reload.text = reload
        viewHolder.itemView.equipment_edit_item_weapon_traits.text = traits
    }

    override fun getLayout(): Int {
        return R.layout.equipment_edit_item_weapon
    }
}