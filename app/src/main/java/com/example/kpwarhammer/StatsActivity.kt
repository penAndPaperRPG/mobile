package com.example.kpwarhammer

import android.app.ActionBar
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.action_bar_layout.*
import kotlinx.android.synthetic.main.activity_stats.*
import kotlinx.android.synthetic.main.stats_item.view.*
import java.lang.ClassCastException
import java.lang.IndexOutOfBoundsException
import kotlin.reflect.KClass

class StatsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stats)
        val id = intent.getStringExtra(KPSelectorActivity.ID_KEY)
        supportActionBar?.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM)
        supportActionBar?.setCustomView(R.layout.action_bar_layout)
        val textView = findViewById<TextView>(R.id.action_bar_layout_title)
        textView.text = "Statystyki"
        val button = findViewById<Button>(R.id.action_bar_layout_button)
        button.text = "Edytuj"
        action_bar_layout_button.setOnClickListener {
            val intent = Intent(this, StatsEditActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        action_bar_layout_next.setOnClickListener {
            val intent = Intent(this, AbilitiesActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        action_bar_layout_previous.setOnClickListener {
            val intent = Intent(this, WealthActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }
        fetchStats(id)
    }

    companion object {
        val ID_KEY = "ID_KEY"
    }

    fun fetchStats(id: String) {
        val adapter = GroupAdapter<ViewHolder>()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val db = FirebaseFirestore.getInstance()
        val stats = arrayOf(
            "melee",
            "range",
            "strength",
            "toughness",
            "agility",
            "intelligence",
            "will_power",
            "fellowship",
            "attacks",
            "wounds",
            "strength_bonus",
            "toughness_bonus",
            "movement",
            "magic",
            "insanity",
            "fortune"
        )
        val names: Array<String> = arrayOf(
            "Walka wręcz",
            "Umiejętności strzeleckie",
            "Krzepa",
            "Odporność",
            "Zręczność",
            "Inteligencja",
            "Siła woli",
            "Ogłada",
            "Ataki",
            "Żywotność",
            "Siła",
            "Wytrzymałość",
            "Szybkość",
            "Magia",
            "Punkty obledu",
            "Punkty przeznaczenia"
        )
        db.collection(email).document(id)
            .get()
            .addOnSuccessListener { result ->
                var i = 0
                stats.forEach {
                    if (result.get(it) != null) {
                        val list = result.get(it) as ArrayList<String>
                        val lastIndex = list.size
                        Log.d("OOGA", "rozmiar $lastIndex")
                        var starting: String = "0"
                        var scheme: String = "0"
                        var current: String = "0"
                        list.forEach {
                            try {
                                starting = list.get(0)
                            } catch (e: IndexOutOfBoundsException) {
                            }
                            try {
                                scheme = list.get(1)
                            } catch (e: IndexOutOfBoundsException) {
                            }
                            try {
                                current = list.get(2)
                            } catch (e: IndexOutOfBoundsException) {

                            }
                            var error = false
                            try {
                                if(starting=="null" || starting=="true" || starting=="false")starting="0"
                                if(scheme=="null" || scheme=="true" || scheme=="false")scheme="0"
                                if(current=="null" || current=="true" || current=="false")current="0"
                                val statsItem = StatsItem(names[i], starting, current, scheme)
                            } catch (e: ArrayIndexOutOfBoundsException) {
                                error = true
                            }
                            if (!error) {
                                val statsItem = StatsItem(names[i], starting, current, scheme)
                                adapter.add(StatsValueItem(statsItem))
                            } else {
                            }
                            i++
                        }
                    }
                }
            }
            .addOnFailureListener {

            }
        recyclerview_stats.adapter = adapter
    }
}

class StatsItem(val value: String, val starting: String, val scheme: String, val current: String)

class StatsValueItem(private val statsItem: StatsItem) : Item<ViewHolder>() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.stats_item_name.text = statsItem.value
        viewHolder.itemView.stats_item_starting.text = statsItem.starting
        viewHolder.itemView.stats_item_scheme.text = statsItem.scheme
        viewHolder.itemView.stats_item_current.text = statsItem.current
    }

    override fun getLayout(): Int {
        return R.layout.stats_item
    }
}
