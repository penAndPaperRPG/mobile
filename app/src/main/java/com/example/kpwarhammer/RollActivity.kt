package com.example.kpwarhammer

import android.app.ActionBar
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.action_bar_layout.*
import kotlinx.android.synthetic.main.activity_roll.*
import kotlin.random.Random

class RollActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_roll)
        val id = intent.getStringExtra(KPSelectorActivity.ID_KEY)
        supportActionBar?.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM)
        supportActionBar?.setCustomView(R.layout.action_bar_layout)
        val textView = findViewById<TextView>(R.id.action_bar_layout_title)
        textView.text = "Rzut kością"
        val button = findViewById<Button>(R.id.action_bar_layout_button)
        button.text = "???"

//        action_bar_layout_button.setOnClickListener{
//            val intent = Intent(this, RollEditActivity::class.java)
//            intent.putExtra(KPSelectorActivity.ID_KEY, id)
//            startActivity(intent)
//        }

        action_bar_layout_next.setOnClickListener{
            val intent = Intent(this, BioActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        action_bar_layout_previous.setOnClickListener{
            val intent = Intent(this, EquipmentActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        roll_button.setOnClickListener{
            val random = (1..100).random()
            roll_textview.setText(random.toString())
            if(random<50) roll_textview.setTextColor(Color.RED)
            else if(random>50) roll_textview.setTextColor(Color.GREEN)
            else roll_textview.setTextColor(Color.BLACK)
        }

    }
}
