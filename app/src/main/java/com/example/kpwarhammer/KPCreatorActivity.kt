package com.example.kpwarhammer

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_kpcreator.*
import java.util.*

class KP1Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kpcreator)
        supportActionBar?.title = "Kreator karty postaci"
        selectphoto_button_kp1.setOnClickListener{
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

        create_button_kp1.setOnClickListener{
            val characterImageUrl = uploadImageToFirebaseStorage()
            saveKPToFirebaseDatabase(characterImageUrl)
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }
    }

    private var selectedPhotoUri: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null){
            selectedPhotoUri = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)
            selectphoto_imageview_kp1.setImageBitmap(bitmap)
            selectphoto_button_kp1.alpha = 0f
        }
    }

    private fun uploadImageToFirebaseStorage(): String {
        if(selectedPhotoUri == null) return null.toString()
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")
        val url = "/images/$filename"
        ref.putFile(selectedPhotoUri!!)
            .addOnFailureListener{
                Toast.makeText(this, "Nie udalo sie załadować obrazka!", Toast.LENGTH_SHORT).show()
            }
        return url
    }

    private fun saveKPToFirebaseDatabase(characterImageUrl: String){
        val db = FirebaseFirestore.getInstance()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        var age = "0"
        var height = "0"
        var weight = "0"

            age = age_edittext_kp1.text.toString()
            height = height_edittext_kp1.text.toString()
            weight = weight_edittext_kp1.text.toString()

        val kp = KP(name_edittext_kp1.text.toString(),
            characterImageUrl,
            race_edittext_kp1.text.toString(),
            profession_edittext_kp1.text.toString(),
            gender_edittext_kp1.text.toString(),
            age,
            height,
            weight,
            eyes_edittext_kp1.text.toString())

        db.collection(email)
            .add(kp)
            .addOnFailureListener{
                Toast.makeText(this, "Nie udalo sie dodac karty postaci do bazy! Błąd: ${it.message}", Toast.LENGTH_SHORT).show()
            }
    }
}

class KP(
    val name: String,
    val character_image: String,
    val race: String, val profession: String,
    val gender: String,
    val age: String,
    val height: String,
    val weight: String,
    val eyes: String
)