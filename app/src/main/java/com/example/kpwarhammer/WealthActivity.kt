package com.example.kpwarhammer

import android.app.ActionBar
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.action_bar_layout.*
import kotlinx.android.synthetic.main.activity_wealth.*
import kotlinx.android.synthetic.main.wealth_item.view.*

class WealthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wealth)
        val id = intent.getStringExtra(KPSelectorActivity.ID_KEY)
        supportActionBar?.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM)
        supportActionBar?.setCustomView(R.layout.action_bar_layout)
        val textView = findViewById<TextView>(R.id.action_bar_layout_title)
        textView.text = "Plecak"
        val button = findViewById<Button>(R.id.action_bar_layout_button)
        button.text = "Edytuj"
        action_bar_layout_button.setOnClickListener{
            val intent = Intent(this, WealthEditActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            Log.d("OOGA", "Przed start activity")
            startActivity(intent)
        }

        action_bar_layout_previous.setOnClickListener{
            val intent = Intent(this, BioActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        action_bar_layout_next.setOnClickListener{
            val intent = Intent(this, StatsActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }
        recyclerview_wealth.findViewHolderForLayoutPosition(0)?.itemView?.wealth_item_edittext?.isEnabled=false
        fetchWealth(id)
    }

    companion object {
        val ID_KEY = "ID_KEY"
    }

    private fun fetchWealth(id: String) {
        val adapter = GroupAdapter<ViewHolder>()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val db = FirebaseFirestore.getInstance()
        var i=1
        db.collection(email).document(id)
            .get()
            .addOnSuccessListener { result ->
                if(result.get("wealth")!=null) {
                    val wealth=result.get("wealth").toString().removePrefix("[").removeSuffix("]")
                    adapter.add(WealthValueItem(wealth))
                }
                else{
                    adapter.add(WealthValueItem(""))

                }
                recyclerview_wealth.adapter = adapter
                var gold = result.get("gold").toString()
                if(gold=="null")gold="0"
                var silver = result.get("silver").toString()
                if(silver=="null")silver="0"
                var bronze = result.get("bronze").toString()
                if(bronze=="null")bronze="0"
                wealth_gold_value.setText(gold)
                wealth_silver_value.setText(silver)
                wealth_bronze_value.setText(bronze)
                }
            .addOnFailureListener{
                Toast.makeText(this, "Nie udało się pobrać karty postaci! Błąd: $it", Toast.LENGTH_SHORT).show()
            }
    }
}


class WealthValueItem(private val wealthItem: String): Item<ViewHolder>(){
    val wealthItem2 = Editable.Factory.getInstance().newEditable(wealthItem.toString())


    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.wealth_item_edittext.text = wealthItem2
        viewHolder.itemView.wealth_item_edittext.hint = "nie masz obecnie żadnych przedmiotów..."
    }
    override fun getLayout(): Int {
        return R.layout.wealth_item
    }
}
