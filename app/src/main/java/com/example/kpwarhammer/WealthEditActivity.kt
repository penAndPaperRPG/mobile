package com.example.kpwarhammer

import android.app.ActionBar
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.action_bar_layout.*
import kotlinx.android.synthetic.main.action_bar_layout.view.*
import kotlinx.android.synthetic.main.activity_wealth_edit.*
import kotlinx.android.synthetic.main.wealth_edit_item.view.wealth_edit_item_name
import kotlinx.android.synthetic.main.wealth_edit_item_money.view.*

class WealthEditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wealth_edit)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.action_bar_layout)
        val textView = findViewById<TextView>(R.id.action_bar_layout_title)
        textView.text = "Edycja plecaka"
        val button = findViewById<Button>(R.id.action_bar_layout_button)
        button.text = "Zapisz"
        val id = intent.getStringExtra(KPSelectorActivity.ID_KEY)
        val returnValue = intent.getStringExtra(AddItemActivity.RETURN_KEY)
        val activity = "wealth"
        val wealth : MutableMap<String, Any> = mutableMapOf<String, Any>()

        action_bar_layout_button.setOnClickListener{
            save(id, wealth)
        }

        fetchWealthEdit(id)
    }






    private fun save(id: String, wealth: MutableMap<String, Any>){
        val db = FirebaseFirestore.getInstance()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val wealthContainer : ArrayList<String> = ArrayList<String>()
        //val money : MutableMap<String, Any> = mutableMapOf<String, Any>()
        wealth["gold"]="0"
        wealth["silver"]="0"
        wealth["bronze"]="0"
        var flag = false
        for(value in 0 until recyclerview_wealth_edit.adapter!!.itemCount-3){
            val recordValue = recyclerview_wealth_edit.findViewHolderForLayoutPosition(value)?.itemView?.wealth_edit_item_name?.text.toString()
            if(recordValue!="" && recordValue!="null") {
                wealthContainer.add(recordValue)
            }
        }
        wealth["wealth"]=wealthContainer
        val itemCount = recyclerview_wealth_edit.adapter!!.itemCount-3
        val gold = recyclerview_wealth_edit.findViewHolderForLayoutPosition(itemCount)?.itemView?.wealth_edit_item_name?.text.toString()
        val silver = recyclerview_wealth_edit.findViewHolderForLayoutPosition(itemCount+1)?.itemView?.wealth_edit_item_name?.text.toString()
        val bronze = recyclerview_wealth_edit.findViewHolderForLayoutPosition(itemCount+2)?.itemView?.wealth_edit_item_name?.text.toString()


        if(gold!=""){
            try {
                wealth["gold"] = gold.toInt()
            }
            catch (e: NumberFormatException){
                Log.d("OOGA", "kacz gold")
                wealth["gold"]="0"
        }

        if(silver!=""){
            try {
                wealth["silver"] = silver.toInt()
            }
            catch (e: NumberFormatException){
                wealth["silver"]="0"
            }
        }

        if(bronze!=""){
            try {
                wealth["bronze"] = bronze.toInt()
            }
            catch (e: NumberFormatException){
                wealth["bronze"]="0"}
        }

        if(!flag){
            db.collection(email).document(id).update(wealth)
            Toast.makeText(this, "Zapisano!", Toast.LENGTH_SHORT).show()
        }
        else{
            Toast.makeText(this, "Nie zapisano! Złoto/srebro/bronz musi być wartością liczbową!", Toast.LENGTH_SHORT).show()
        }
    }


}
    fun fetchWealthEdit(id: String){
        val adapter = GroupAdapter<ViewHolder>()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val db = FirebaseFirestore.getInstance()
        db.collection(email).document(id)
            .get()
            .addOnSuccessListener { result ->
                if(result.get("wealth")!=null && result.get("wealth")!="null"){
                    val list = result.get("wealth") as ArrayList<Any>
                    val wealth = list.toString().removePrefix("[").removeSuffix("]")
                    adapter.add(WealthEditValueItem(wealth))
                }
                else {
                    adapter.add(WealthEditValueItem(""))
                }

                val gold = result.get("gold").toString()
                val silver = result.get("silver").toString()
                val bronze = result.get("bronze").toString()

                if(gold!="null"){
                    adapter.add(WealthEditMoneyItem(gold, "Gold"))
                }
                else {
                    adapter.add(WealthEditMoneyItem("0", "Gold"))
                }
                if(silver!="null"){
                    adapter.add(WealthEditMoneyItem(silver, "Silver"))
                }
                else {
                    adapter.add(WealthEditMoneyItem("0", "Silver"))
                }
                if(bronze!="null"){
                    adapter.add(WealthEditMoneyItem(bronze, "Bronze"))
                }
                else {
                    adapter.add(WealthEditMoneyItem("0", "Bronze"))
                }


                recyclerview_wealth_edit.adapter = adapter
            }
    }
}

class WealthEditMoneyItem(private val value: String, private val type: String): Item<ViewHolder>(){

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.wealth_edit_item_name.setText(value)
        viewHolder.itemView.wealth_edit_item_name.hint = type
        viewHolder.itemView.wealth_edit_item_money_textview.setText(type)
    }
    override fun getLayout(): Int {
        return R.layout.wealth_edit_item_money
    }
}


class WealthEditValueItem(private val item: String): Item<ViewHolder>(){

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.wealth_edit_item_name.setText(item)
    }
    override fun getLayout(): Int {
        return R.layout.wealth_edit_item
    }
}
