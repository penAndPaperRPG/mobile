package com.example.kpwarhammer

import android.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.action_bar_layout.*
import kotlinx.android.synthetic.main.activity_stats_edit.*
import kotlinx.android.synthetic.main.activity_wealth_edit.*
import kotlinx.android.synthetic.main.stats_edit_item.view.*
import kotlinx.android.synthetic.main.wealth_edit_item.view.*
import kotlinx.android.synthetic.main.wealth_edit_item.view.wealth_edit_item_name
import kotlinx.android.synthetic.main.wealth_edit_item_money.view.*
import java.lang.IndexOutOfBoundsException
import java.lang.NumberFormatException

class StatsEditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stats_edit)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.action_bar_layout)
        val textView = findViewById<TextView>(R.id.action_bar_layout_title)
        textView.text = "Edycja statystyk"
        val button = findViewById<Button>(R.id.action_bar_layout_button)
        button.text = "Zapisz"
        val id = intent.getStringExtra(KPSelectorActivity.ID_KEY)
        val statsMap: MutableMap<String, Any> = mutableMapOf<String, Any>()

        action_bar_layout_button.setOnClickListener {
            save(id, statsMap)
        }
        fetchStats(id)
    }

    fun fetchStats(id: String) {
        val adapter = GroupAdapter<ViewHolder>()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val db = FirebaseFirestore.getInstance()
        val stats: Array<String> = arrayOf(
            "melee",
            "range",
            "strength",
            "toughness",
            "agility",
            "intelligence",
            "will_power",
            "fellowship",
            "attacks",
            "wounds",
            "strength_bonus",
            "toughness_bonus",
            "movement",
            "magic",
            "insanity",
            "fortune"
        )
        val names: Array<String> = arrayOf(
            "Walka wręcz",
            "Umiejętności strzeleckie",
            "Krzepa",
            "Odporność",
            "Zręczność",
            "Inteligencja",
            "Siła woli",
            "Ogłada",
            "Ataki",
            "Żywotność",
            "Siła",
            "Wytrzymałość",
            "Szybkość",
            "Magia",
            "Punkty obłędu",
            "Punkty przeznaczenia"
        )
        var i = 0
        db.collection(email).document(id)
            .get()
            .addOnSuccessListener { result ->
                stats.forEach {
                    if (result.get(it) != null) {
                        val list = result.get(it) as ArrayList<String>
                        var starting = "0"
                        var scheme = "0"
                        var current = "0"
                        try {
                            starting = list.get(0)
                        } catch (e: IndexOutOfBoundsException) {
                        }
                        try {
                            starting = list.get(1)
                        } catch (e: IndexOutOfBoundsException) {
                        }
                        try {
                            starting = list.get(2)
                        } catch (e: IndexOutOfBoundsException) {
                        }
                        adapter.add(StatsEditValueItem(StatsEditItem(names[i], starting, scheme, current)))

                    } else {
                        adapter.add(StatsEditValueItem(StatsEditItem(names[i], "false", "false", "false")))
                    }
                    i++

                }
                recyclerview_stats_edit.adapter = adapter
            }
    }


private fun save(id: String, statsMap: MutableMap<String, Any>) {
    val db = FirebaseFirestore.getInstance()
    val email = FirebaseAuth.getInstance().currentUser?.email.toString()
    val id = id
    val stats: Array<String> = arrayOf(
        "melee",
        "range",
        "strength",
        "toughness",
        "agility",
        "intelligence",
        "will_power",
        "fellowship",
        "attacks",
        "wounds",
        "strength_bonus",
        "toughness_bonus",
        "movement",
        "magic",
        "insanity",
        "fortune"
    )
    var flag = false
    for (value in 0 until recyclerview_stats_edit.adapter!!.itemCount) {
        val stat = stats[value]
        val starting = recyclerview_stats_edit.findViewHolderForLayoutPosition(value)
            ?.itemView?.stats_edit_item_starting?.text.toString()
        val scheme = recyclerview_stats_edit.findViewHolderForLayoutPosition(value)
            ?.itemView?.stats_edit_item_scheme?.text.toString()
        val current = recyclerview_stats_edit.findViewHolderForLayoutPosition(value)
            ?.itemView?.stats_edit_item_current?.text.toString()
        val values = listOf<String>(starting, scheme, current)
        statsMap[stats[value]] = values
        db.collection(email).document(id).update(statsMap)
            .addOnSuccessListener {
                Toast.makeText(this, "Zapisano!", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener {
                Toast.makeText(this, "Nie udało się zapisać! Błąd: $it", Toast.LENGTH_SHORT).show()
            }
    }

}
}

class StatsEditItem(val name: String, val starting: String, val scheme: String, val current: String)

class StatsEditValueItem(statsEditItem: StatsEditItem) : Item<ViewHolder>() {
    val starting = Editable.Factory.getInstance().newEditable(statsEditItem.starting)
    val scheme = Editable.Factory.getInstance().newEditable(statsEditItem.scheme)
    val current = Editable.Factory.getInstance().newEditable(statsEditItem.current)
    val name = statsEditItem.name

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.stats_edit_item_starting.text = starting
        viewHolder.itemView.stats_edit_item_scheme.text = scheme
        viewHolder.itemView.stats_edit_item_current.text = current
        viewHolder.itemView.stats_edit_item_name.setText(name)
    }

    override fun getLayout(): Int {
        return R.layout.stats_edit_item
    }
}