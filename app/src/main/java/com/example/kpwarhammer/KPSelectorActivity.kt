package com.example.kpwarhammer

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_kpselector.*
import kotlinx.android.synthetic.main.kp_list_item.view.*

class KPSelectorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kpselector)
        supportActionBar?.title = "Wybierz kartę postaci"
        fetchKPs()
    }

    companion object {
        val ID_KEY = "ID_KEY"
    }

    private fun fetchKPs(){
        val adapter = GroupAdapter<ViewHolder>()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val db = FirebaseFirestore.getInstance()
        db.collection(email)
            .get()
            .addOnSuccessListener { result->
                for(document in result){
                    var name="brak imienia"
                    var race="brak rasy"
                    var profession="brak profesji"
                    if(document.get("name")!=null && document.get("name")!="null" ){
                        name = document.get("name").toString()
                    }
                    if(document.get("race")!=null && document.get("race")!="null" ){
                        name = document.get("race").toString()
                    }
                    if(document.get("profession")!=null && document.get("profession")!="null" ){
                        name = document.get("profession").toString()
                    }


                    val kpBasic =  KPBasic(
                        name,
                        race,
                        profession,
                        document.get("character_image").toString(),
                        document.id
                    )
                    adapter.add(KPItem(kpBasic))
                }

                adapter.setOnItemClickListener { item, view ->
                    val kpItem = item as KPItem
                    val intent = Intent(view.context, BioActivity::class.java)
                    intent.putExtra(ID_KEY, kpItem.kpBasic.id)
                    startActivity(intent)
                    finish()
                }
                kplist_recyclerview_kpselector.adapter = adapter
            }
            .addOnFailureListener{
                Toast.makeText(this, "Nie udało się pobrać kart postaci!", Toast.LENGTH_LONG).show()
            }
    }
}

class KPBasic(val name: String, val race: String, val profession: String, val character_image: String, val id: String)

class KPItem(val kpBasic: KPBasic): Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.kpname.text = "${kpBasic.name}, ${kpBasic.race}, ${kpBasic.profession}"
        val storage = FirebaseStorage.getInstance()
        val path = kpBasic.character_image
        val storageRef = storage.getReference()
        storageRef.child(path).downloadUrl.addOnSuccessListener {
            Picasso.get().load(it).into(viewHolder.itemView.kpimage)
        }
            .addOnFailureListener {
            }
    }
    override fun getLayout(): Int {
        return R.layout.kp_list_item
    }
}