package com.example.kpwarhammer

import android.app.ActionBar
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.kpwarhammer.R.id
import com.example.kpwarhammer.R.layout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.action_bar_layout.*
import kotlinx.android.synthetic.main.activity_bio_edit.*
import kotlinx.android.synthetic.main.bio_edit_item.view.*
import kotlin.NumberFormatException as NumberFormatException


class BioEditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_bio_edit)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(layout.action_bar_layout)
        val textView = findViewById<TextView>(id.action_bar_layout_title)
        textView.text = "Edycja bio"
        val button = findViewById<Button>(id.action_bar_layout_button)
        button.text = "Zapisz"
        val id = intent.getStringExtra(KPSelectorActivity.ID_KEY)
        action_bar_layout_button.setOnClickListener{
            save(id)
        }
        fetchBioEdit(id)
    }


    private fun save(id: String){
        val db = FirebaseFirestore.getInstance()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val map = mutableMapOf<String?, Any?>()
        val records: Array<String> = arrayOf(
            "name",
            "race",
            "profession",
            "experience",
            "all_experience",
            "gender",
            "age",
            "height",
            "weight",
            "eyes",
            "hair",
            "star_sign",
            "marks",
            "previous_profession",
            "birthplace",
            "family",
            "social_status",
            "past",
            "motivation",
            "serves",
            "friends",
            "foes",
            "likes",
            "dislikes",
            "personality",
            "goals",
            "game_master",
            "campaign")
        var flag = false
        for(value in 0 until recyclerview_bio_edit.adapter!!.itemCount) {
            val recordValue = recyclerview_bio_edit.findViewHolderForLayoutPosition(value)?.itemView?.bio_edit_edittext?.text.toString()
            map[records[value]] = recordValue
            }
            db.collection(email).document(id).update(map)
                .addOnSuccessListener {
                    Toast.makeText(this, "Zapis udany!", Toast.LENGTH_SHORT).show() }
                .addOnFailureListener {exception ->
                    Toast.makeText(this, "Zapis nieudany! Błąd: $exception", Toast.LENGTH_SHORT).show()
                }
        fetchBioEdit(id)
    }

    private fun fetchBioEdit(id: String){
        val adapter = GroupAdapter<ViewHolder>()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val db = FirebaseFirestore.getInstance()
        val bio: Array<Pair> = arrayOf(
            Pair("Imię", "name"),
            Pair("Rasa", "race"),
            Pair("Profesja", "profession"),
            Pair("Doświadczenie", "experience"),
            Pair("Całe doświadczenie", "all_experience"),
            Pair("Płeć", "gender"),
            Pair("Wiek", "age"),
            Pair("Wzrost", "height"),
            Pair("Waga", "weight"),
            Pair("Oczy", "eyes"),
            Pair("Włosy", "hair"),
            Pair("Znak gwiezdny", "star_sign"),
            Pair("Znaki szczególne", "marks"),
            Pair("Poprzednia profesja", "previous_profession"),
            Pair("Pochodzenie", "birthplace"),
            Pair("Rodzina", "family"),
            Pair("Pozycja społeczna", "social_status"),
            Pair("Kim wcześniej był", "past"),
            Pair("Dlaczego wyruszył na szlak", "motivation"),
            Pair("Komu służy", "serves"),
            Pair("Przyjaciele", "friends"),
            Pair("Wrogowie", "foes"),
            Pair("Co lubi", "likes"),
            Pair("Czego nie lubi", "dislikes"),
            Pair("Osobowość i zachowania", "personality"),
            Pair("Motywacja, cele, ambicje", "goals"),
            Pair("Mistrz gry", "game_master"),
            Pair("Kampania", "campaign"))
        db.collection(email).document(id)
            .get()
            .addOnSuccessListener { result ->
                bio.forEach {
                    if(result.get(it.name)!=null && result.get(it.name)!="null"){
                    val result = result.get(it.name).toString()
                        val bioEditValue = BioEditValue(type = it.nazwa, value = result)
                        adapter.add(BioEditValueItem(bioEditValue))
                    }
                else {val bioEditValue = BioEditValue(type=it.nazwa, value="")
                    adapter.add(BioEditValueItem(bioEditValue))}
                }
                this.recyclerview_bio_edit.adapter = adapter
                }
            .addOnFailureListener{
                Toast.makeText(this, "Nie udało się pobrać karty postaci!", Toast.LENGTH_SHORT).show()
            }
    }
}

class BioEditValue (val type: String, val value: String)

class BioEditValueItem(private val bioEditValue: BioEditValue): Item<ViewHolder>(){

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.bio_edit_edittext.setText(bioEditValue.value)
        viewHolder.itemView.bio_edit_edittext.hint = bioEditValue.type
    }
    override fun getLayout(): Int {
        return R.layout.bio_edit_item
    }
}