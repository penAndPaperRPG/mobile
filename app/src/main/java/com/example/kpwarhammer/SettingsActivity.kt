package com.example.kpwarhammer

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        verifyUserIsLoggedIn()

        supportActionBar?.title = "Ustawienia"

        logout_button_settings.setOnClickListener{
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        newkp_button_settings.setOnClickListener{
            val intent = Intent(this, KP1Activity::class.java)
            startActivity(intent)
        }

        kpselector_button_settings.setOnClickListener{
            val intent = Intent(this, KPSelectorActivity::class.java)
            startActivity(intent)
        }
    }
    private fun verifyUserIsLoggedIn(){
        val uid = FirebaseAuth.getInstance().uid
        if (uid == null) {
            val intent = Intent(this, RegisterActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }
}
