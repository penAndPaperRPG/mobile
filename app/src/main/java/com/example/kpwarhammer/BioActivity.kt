package com.example.kpwarhammer

import android.app.ActionBar
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.kpwarhammer.Models.KPfetcher
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.action_bar_layout.*
import kotlinx.android.synthetic.main.activity_bio.*
import kotlinx.android.synthetic.main.bio_item.view.*
import kotlinx.android.synthetic.main.image_item.view.*

class BioActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bio)
        val id = intent.getStringExtra(KPSelectorActivity.ID_KEY)
        supportActionBar?.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM)
        supportActionBar?.setCustomView(R.layout.action_bar_layout)
        val textView = findViewById<TextView>(R.id.action_bar_layout_title)
        textView.text = "Bio"
        val button = findViewById<Button>(R.id.action_bar_layout_button)
        button.text = "Edytuj"
        action_bar_layout_button.setOnClickListener{
            val intent = Intent(this, BioEditActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        action_bar_layout_next.setOnClickListener{
            val intent = Intent(this, WealthActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        action_bar_layout_previous.setOnClickListener{
            val intent = Intent(this, RollActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

//        val ooga : KPfetcher = KPfetcher(id)
//        ooga.fetchKP("bio")
        fetchBio(id)
    }

    companion object {
        val ID_KEY = "ID_KEY"
    }

    private fun fetchBio(id: String){
        val adapter = GroupAdapter<ViewHolder>()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val db = FirebaseFirestore.getInstance()
        val bio: Array<Pair> = arrayOf(
            Pair("Imię", "name"),
            Pair("Rasa", "race"),
            Pair("Profesja", "profession"),
            Pair("Doświadczenie", "experience"),
            Pair("Całe doświadczenie", "all_experience"),
            Pair("Płeć", "gender"),
            Pair("Wiek", "age"),
            Pair("Wzrost", "height"),
            Pair("Waga", "weight"),
            Pair("Oczy", "eyes"),
            Pair("Włosy", "hair"),
            Pair("Znak gwiezdny", "star_sign"),
            Pair("Znaki szczególne", "marks"),
            Pair("Poprzednia profesja", "previous_profession"),
            Pair("Pochodzenie", "birthplace"),
            Pair("Rodzina", "family"),
            Pair("Pozycja społeczna", "social_status"),
            Pair("Kim wcześniej był", "past"),
            Pair("Dlaczego wyruszył na szlak", "motivation"),
            Pair("Komu służy", "serves"),
            Pair("Przyjaciele", "friends"),
            Pair("Wrogowie", "foes"),
            Pair("Co lubi", "likes"),
            Pair("Czego nie lubi", "dislikes"),
            Pair("Osobowość i zachowania", "personality"),
            Pair("Motywacja, cele, ambicje", "goals"),
            Pair("Mistrz gry", "game_master"),
            Pair("Kampania", "campaign"))
        db.collection(email).document(id)
            .get()
            .addOnSuccessListener { result ->
                val path = result.get("character_image").toString()
                if(path!="" && path!="null"){
                    val image = BioImage(
                        path
                    )
                    adapter.add(BioImageItem(image))
                }
                bio.forEach {
                    val result = result.get(it.name).toString()
                    if(result!="" && result!="null" && result!="0"){
                        val bioValue = BioValue(
                            it.nazwa,
                            result
                        )
                    adapter.add(BioValueItem(bioValue))
                  }
                }
                recyclerview_bio.adapter = adapter
            }
            .addOnFailureListener{
                Toast.makeText(this, "Nie udało się pobrać karty postaci!", Toast.LENGTH_SHORT).show()
            }
        }
    }

class BioValue (val type: String, val value: String)

class BioValueItem(private val bioValue: BioValue): Item<ViewHolder>(){

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.value_item_name.text = bioValue.type
        viewHolder.itemView.value_item_value.text = bioValue.value
    }
    override fun getLayout(): Int {
        return R.layout.bio_item
    }
}

class BioImage (val character_image: String)

class BioImageItem(private val bioImage: BioImage): Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {
        val storage = FirebaseStorage.getInstance()
        val path = bioImage.character_image
        val storageRef = storage.getReference()
        storageRef.child(path).downloadUrl.addOnSuccessListener {
            Picasso.get().load(it).into(viewHolder.itemView.image_item_image)
        }
            .addOnFailureListener{
            }
    }

    override fun getLayout(): Int {
        return R.layout.image_item
    }
}

class Pair (val nazwa: String, val name: String)