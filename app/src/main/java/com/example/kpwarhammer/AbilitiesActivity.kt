package com.example.kpwarhammer

import android.app.ActionBar
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.abilities_item.view.*
import kotlinx.android.synthetic.main.action_bar_layout.*
import kotlinx.android.synthetic.main.activity_abilities.*

class AbilitiesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_abilities)
        val id = intent.getStringExtra(KPSelectorActivity.ID_KEY)
        supportActionBar?.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM)
        supportActionBar?.setCustomView(R.layout.action_bar_layout)
        val textView = findViewById<TextView>(R.id.action_bar_layout_title)
        textView.text = "Umiejętności"
        val button = findViewById<Button>(R.id.action_bar_layout_button)
        button.text = "Edytuj"
        action_bar_layout_button.setOnClickListener {
            val intent = Intent(this, AbilitiesEditActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        action_bar_layout_next.setOnClickListener {
            val intent = Intent(this, EquipmentActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        action_bar_layout_previous.setOnClickListener {
            val intent = Intent(this, StatsActivity::class.java)
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }

        fetchAbilities(id)
    }

    fun fetchAbilities(id: String) {
        val adapter = GroupAdapter<ViewHolder>()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val db = FirebaseFirestore.getInstance()
        val abilities: Array<String> = arrayOf(
            "command",
            "gamble",
            "ride",
            "drinking",
            "animals",
            "gossip",
            "swim",
            "drive",
            "charm",
            "search",
            "hiding",
            "perception",
            "survival",
            "haggle",
            "hiding",
            "rowing",
            "climbing",
            "evaluate",
            "intimidation",
            "knowledge",
            "language"
        )
        val names: Array<String> = arrayOf(
            "Dowodzenie",
            "Hazard",
            "Jeździectwo",
            "Mocna głowa",
            "Opieka nad zwierzętami",
            "Plotkowanie",
            "Pływanie",
            "Powożenie",
            "Przekonywanie",
            "Przeszukiwanie",
            "Skradanie się",
            "Spostrzegawczość",
            "Sztuka przetrwania",
            "Targowanie",
            "Ukrywanie się",
            "Wioślarstwo",
            "Wspinaczka",
            "Wycena",
            "Zastraszanie",
            "Wiedza",
            "Język"
        )
        val attrs: Array<String> = arrayOf(
            "Ogd",
            "Int",
            "Zr",
            "Odp",
            "Int",
            "Ogd",
            "K",
            "K",
            "Ogd",
            "Int",
            "Zr",
            "Int",
            "Int",
            "Ogd",
            "Zr",
            "K",
            "K",
            "Int",
            "K",
            "Int",
            "Int"
        )
        db.collection(email).document(id)
            .get()
            .addOnSuccessListener { result ->
                var i = 0
                abilities.forEach {
                    if(result.get(it)!=null){
                    val skills = result.get(it) as ArrayList<Any?>
                        val w:Boolean=false
                        val ten:Boolean=false
                        val twenty:Boolean=false
                        try{val w: Boolean = skills.get(0).toString().toBoolean()}
                        catch(e: IndexOutOfBoundsException){}
                        try{val ten: Boolean = skills.get(1).toString().toBoolean()}
                        catch(e: IndexOutOfBoundsException){}
                        try{val twenty: Boolean = skills.get(2).toString().toBoolean()}
                        catch(e: IndexOutOfBoundsException){}
                        val abilitiesItem = AbilitiesItem(names[i], w, ten, twenty, attrs[i])
                        adapter.add(AbilitiesValueItem(abilitiesItem))
                        i++

                }
                else{
                        adapter.add(AbilitiesValueItem(AbilitiesItem(names[i], false, false, false, attrs[i])))
                    }
                }
            }
        recyclerview_abilities.adapter = adapter
    }
}


class AbilitiesItem(val name: String, val w: Boolean, val ten: Boolean, val twenty: Boolean, val attr: String)

class AbilitiesValueItem(private val abilitiesItem: AbilitiesItem) : Item<ViewHolder>() {
    val twenty = abilitiesItem.twenty
    val ten = abilitiesItem.ten
    val w = abilitiesItem.w
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.abilities_item_attr.text = abilitiesItem.attr
        viewHolder.itemView.abilities_item_name.text = abilitiesItem.name
        if (twenty) {
            viewHolder.itemView.abilities_item_state.text = "+20"
        } else if (ten) {
            viewHolder.itemView.abilities_item_state.text = "+10"
        } else if (w) {
            viewHolder.itemView.abilities_item_state.text = "posiadane"
        } else {
            viewHolder.itemView.abilities_item_state.text = "nieposiadane"
        }
    }

    override fun getLayout(): Int {
        return R.layout.abilities_item
    }
}
