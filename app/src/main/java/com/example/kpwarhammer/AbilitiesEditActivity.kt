package com.example.kpwarhammer

import android.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.abilities_edit_item.view.*
import kotlinx.android.synthetic.main.abilities_edit_item.view.abattr
import kotlinx.android.synthetic.main.abilities_edit_item.view.radiogroup
import kotlinx.android.synthetic.main.abilities_edit_item2.view.*
import kotlinx.android.synthetic.main.action_bar_layout.*
import kotlinx.android.synthetic.main.activity_abilities_edit.*

class AbilitiesEditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_abilities_edit)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.action_bar_layout)
        val textView = findViewById<TextView>(R.id.action_bar_layout_title)
        textView.text = "Edycja umiejętności"
        val button = findViewById<Button>(R.id.action_bar_layout_button)
        button.text = "Zapisz"
        val id = intent.getStringExtra(KPSelectorActivity.ID_KEY)
        val abilitiesMap: MutableMap<String, Any> = mutableMapOf<String, Any>()

        action_bar_layout_button.setOnClickListener {
            save(id, abilitiesMap)
        }
        fetchAbilities(id)
    }

    fun save(id: String, abilitiesMap: MutableMap<String, Any>) {
        val abilities: Array<String> = arrayOf(
            "command",
            "gamble",
            "ride",
            "drinking",
            "animals",
            "gossip",
            "swim",
            "drive",
            "charm",
            "search",
            "hiding",
            "perception",
            "survival",
            "haggle",
            "hiding",
            "rowing",
            "climbing",
            "evaluate",
            "intimidation",
            "knowledge",
            "language"
        )
        val attrs: Array<String> = arrayOf(
            "Ogd",
            "Int",
            "Zr",
            "Odp",
            "Int",
            "Ogd",
            "K",
            "K",
            "Ogd",
            "Int",
            "Zr",
            "Int",
            "Int",
            "Ogd",
            "Zr",
            "K",
            "K",
            "Int",
            "K",
            "Int",
            "Int"
        )
        val db = FirebaseFirestore.getInstance()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        var i = 0
        abilities.forEach { it ->
            if (i < 19) {
                var values: List<Any>
                var radio = recyclerview_abilities_edit.findViewHolderForLayoutPosition(i)
                    ?.itemView?.radiogroup?.checkedRadioButtonId
                val radioValue = (i + 1) * 10 + 4
                if (radio == radioValue) {
                    values = listOf(true, true, true, attrs[i])
                } else if (radio == radioValue - 1) {
                    values = listOf(true, true, false, attrs[i])
                } else if (radio == radioValue - 2) {
                    values = listOf(true, false, false, attrs[i])
                } else {
                    values = listOf(false, false, false, attrs[i])
                }
                abilitiesMap[it] = values
            } else {
                val name = recyclerview_abilities_edit.findViewHolderForLayoutPosition(i)
                    ?.itemView?.abname?.text.toString()
                val radioValue = (i + 1) * 10 + 4
                var values: List<Any>
                var radio = recyclerview_abilities_edit.findViewHolderForLayoutPosition(i)
                    ?.itemView?.radiogroup?.checkedRadioButtonId
                if (radio == radioValue) {
                    values = listOf(name, true, true, true, attrs[i])
                } else if (radio == radioValue - 1) {
                    values = listOf(name, true, true, false, attrs[i])
                } else if (radio == radioValue - 2) {
                    values = listOf(name, true, false, false, attrs[i])
                } else {
                    values = listOf(name, false, false, false, attrs[i])
                }
                abilitiesMap[it] = values
            }
            i++
        }
        db.collection(email).document(id).update(abilitiesMap)
            .addOnSuccessListener {
                Toast.makeText(this, "Zapisano!", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener {
                Toast.makeText(this, "Nie udało się zapisać! Błąd: $it", Toast.LENGTH_SHORT).show()
            }

    }

    fun fetchAbilities(id: String) {
        val abilities: Array<String> = arrayOf(
            "command",
            "gamble",
            "ride",
            "drinking",
            "animals",
            "gossip",
            "swim",
            "drive",
            "charm",
            "search",
            "hiding",
            "perception",
            "survival",
            "haggle",
            "hiding",
            "rowing",
            "climbing",
            "evaluate",
            "intimidation",
            "knowledge",
            "language"
        )
        val names: Array<String> = arrayOf(
            "Dowodzenie",
            "Hazard",
            "Jeździectwo",
            "Mocna głowa",
            "Opieka nad zwierzętami",
            "Plotkowanie",
            "Pływanie",
            "Powożenie",
            "Przekonywanie",
            "Przeszukiwanie",
            "Skradanie się",
            "Spostrzegawczość",
            "Sztuka przetrwania",
            "Targowanie",
            "Ukrywanie się",
            "Wioślarstwo",
            "Wspinaczka",
            "Wycena",
            "Zastraszanie",
            "Wiedza",
            "Język"
        )
        val attrs: Array<String> = arrayOf(
            "Ogd",
            "Int",
            "Zr",
            "Odp",
            "Int",
            "Ogd",
            "K",
            "K",
            "Ogd",
            "Int",
            "Zr",
            "Int",
            "Int",
            "Ogd",
            "Zr",
            "K",
            "K",
            "Int",
            "K",
            "Int",
            "Int"
        )
        val adapter = GroupAdapter<ViewHolder>()
        val email = FirebaseAuth.getInstance().currentUser?.email.toString()
        val id = id
        val db = FirebaseFirestore.getInstance()
        var i = 0
        db.collection(email).document(id)
            .get()
            .addOnSuccessListener { result ->
                abilities.forEach { it ->

                    if (result.get(it) != null) {
                        val list = result.get(it) as ArrayList<Any>
                        if (i < 19) {
                            var radio = "N"
                            if (list.get(2)== true) {
                                radio = "+20"
                            } else if (list.get(1) == true) {
                                radio = "+10"
                            } else if (list.get(0) == true) {
                                radio = "W"
                            }
                            adapter.add(AbilitiesEditValueItem(AbilitiesEditItem(names[i], radio, attrs[i], i + 1)))
                        } else if (i > 18 && i < 21) {
                            var radio = "N"
                            val value = list.get(0)
                            if (list.get(3) == true) {
                                radio = "+20"
                            } else if (list.get(2) == true) {
                                radio = "+10"
                            } else if (list.get(1) == true) {
                                radio = "W"
                            }
                            adapter.add(
                                AbilitiesEditValueItem2(
                                    AbilitiesEditItem2(
                                        names[i],
                                        value.toString(),
                                        radio,
                                        attrs[i],
                                        i + 1
                                    )
                                )
                            )
                        } else {

                            if (i < 19) {
                                adapter.add(AbilitiesEditValueItem(AbilitiesEditItem(names[i], "N", attrs[i], i + 1)))
                            }
                            if (i > 18 && i < 21) {
                                adapter.add(
                                    AbilitiesEditValueItem2(
                                        AbilitiesEditItem2(
                                            names[i],
                                            "",
                                            "N",
                                            attrs[i],
                                            i + 1
                                        )
                                    )
                                )

                            }
                        }
                    } else {
                    }
                    i++
                }
                recyclerview_abilities_edit.adapter = adapter
            }

    }
}


class AbilitiesEditItem(val name: String, val radio: String, val attr: String, val radioId: Int)
class AbilitiesEditItem2(val name: String, val value: String, val radio: String, val attr: String, val radioId: Int)

class AbilitiesEditValueItem2(val abilitiesEditItem: AbilitiesEditItem2) : Item<ViewHolder>() {
    val name = Editable.Factory.getInstance().newEditable(abilitiesEditItem.name.toString())
    val value = Editable.Factory.getInstance().newEditable(abilitiesEditItem.value.toString())
    val attr = Editable.Factory.getInstance().newEditable(abilitiesEditItem.attr.toString())
    val id1 = abilitiesEditItem.radioId * 10 + 1
    val id2 = abilitiesEditItem.radioId * 10 + 2
    val id3 = abilitiesEditItem.radioId * 10 + 3
    val id4 = abilitiesEditItem.radioId * 10 + 4
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.abedittext.text = value//name
        viewHolder.itemView.abedittext.hint = name//value
        viewHolder.itemView.abtextview.setText(attr)
        viewHolder.itemView.radio12.id = id1
        viewHolder.itemView.radio22.id = id2
        viewHolder.itemView.radio32.id = id3
        viewHolder.itemView.radio42.id = id4
        if (abilitiesEditItem.radio == "+20") {
            viewHolder.itemView.radiogroup.check(id4)
        } else if (abilitiesEditItem.radio == "+10") {
            viewHolder.itemView.radiogroup.check(id3)
        } else if (abilitiesEditItem.radio == "W") {
            viewHolder.itemView.radiogroup.check(id2)
        } else {
            viewHolder.itemView.radiogroup.check(id1)
        }
    }

    override fun getLayout(): Int {
        return R.layout.abilities_edit_item2
    }
}

class AbilitiesEditValueItem(val abilitiesEditItem: AbilitiesEditItem) : Item<ViewHolder>() {
    val name = Editable.Factory.getInstance().newEditable(abilitiesEditItem.name.toString())
    val attr = Editable.Factory.getInstance().newEditable(abilitiesEditItem.attr.toString())
    val id1 = abilitiesEditItem.radioId * 10 + 1
    val id2 = abilitiesEditItem.radioId * 10 + 2
    val id3 = abilitiesEditItem.radioId * 10 + 3
    val id4 = abilitiesEditItem.radioId * 10 + 4
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.abname.setText(name)
        viewHolder.itemView.abattr.setText(attr)
        viewHolder.itemView.radio1.id = id1
        viewHolder.itemView.radio2.id = id2
        viewHolder.itemView.radio3.id = id3
        viewHolder.itemView.radio4.id = id4
        if (abilitiesEditItem.radio == "+20") {
            viewHolder.itemView.radiogroup.check(id4)
        } else if (abilitiesEditItem.radio == "+10") {
            viewHolder.itemView.radiogroup.check(id3)
        } else if (abilitiesEditItem.radio == "W") {
            viewHolder.itemView.radiogroup.check(id2)
        } else {
            viewHolder.itemView.radiogroup.check(id1)
        }

    }

    override fun getLayout(): Int {
        return R.layout.abilities_edit_item
    }
}