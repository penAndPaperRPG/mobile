package com.example.kpwarhammer

import android.app.ActionBar
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_add_item.*

class AddItemActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)
        supportActionBar?.title = "Dodaj pozycję"
        val id = intent.getStringExtra(KPSelectorActivity.ID_KEY)
        add_item_button.setOnClickListener{
            val intent = Intent(this, WealthEditActivity::class.java)
            intent.putExtra(RETURN_KEY, add_item_textview.text.toString())
            intent.putExtra(KPSelectorActivity.ID_KEY, id)
            startActivity(intent)
        }
    }

    companion object {
        val RETURN_KEY = "RETURN_VALUE"
        val ID_KEY = "ID_VALUE"
    }


}



